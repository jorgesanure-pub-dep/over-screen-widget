# Over Screen Widget

Check `main.dart` file to see a sample about how to use it.

## Add dependency
Add the following dependency to `pubspec.yaml`
```yaml
dependencies:
  over_screen_widget:
    git:
      url: git@gitlab.com:jorwan-projects/over-screen-widget.git
      ref: 0.0.1
```
## TODO
- [ ] Readme - Description
- [ ] Readme - Getting Started
- [ ] Readme - Oficial Website for verified user in [pub.dev](https://pub.dev/)