import 'dart:async';

abstract class IOverScreenController {
  Function get closeStream;
  Stream get stream;
  bool get isShowing;
  void show();
  void hide();
}
