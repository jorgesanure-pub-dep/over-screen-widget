import 'dart:async';

import 'i_over_screen_controller.dart';

class OverScreenController extends IOverScreenController {
  StreamController _streamController = StreamController.broadcast();

  bool _isShowing;
  bool get isShowing => _isShowing ?? false;

  @override
  void hide() => _streamController.sink.add(_isShowing = false);

  @override
  void show() => _streamController.sink.add(_isShowing = true);

  @override
  Function get closeStream => () => _streamController.close();

  @override
  Stream get stream => _streamController.stream;
}
