import 'package:flutter/material.dart';
import 'package:over_screen_widget/controllers/i_over_screen_controller.dart';
import 'package:over_screen_widget/controllers/over_screen_controller.dart';

class OverScreenWidget extends StatefulWidget {
  final Widget child;
  final Widget overScreenContent;
  final Widget Function(IOverScreenController) builder;
  final bool isShowing;
  final IOverScreenController controller;
  final bool keepChildVisible;

  OverScreenWidget(
      {Key key,
      this.controller,
      this.isShowing,
      this.child,
      this.builder,
      this.overScreenContent,
      this.keepChildVisible = false})
      : super(key: key) {
    assert((child != null && controller != null) || builder != null,
        "'child' and 'controller' should'nt be null OR 'builder' should'nt be null");
  }

  @override
  _OverScreenWidgetState createState() => _OverScreenWidgetState();
}

class _OverScreenWidgetState extends State<OverScreenWidget> {
  IOverScreenController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? OverScreenController();
    if (widget.isShowing ?? false) _controller.show();
  }

  @override
  void dispose() {
    _controller.closeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _child = widget.builder?.call(_controller) ?? widget.child;
    final _overScreenChild = Scaffold(
      backgroundColor: Colors.transparent,
      body: widget.overScreenContent ?? SizedBox.shrink(),
    );

    return StreamBuilder(
      stream: _controller.stream,
      builder: (context, snapshot) {
        final _showOverScreenChild = snapshot.data ?? widget.isShowing ?? false;
        return Stack(
          children: [
            Visibility(
                maintainState: true,
                maintainAnimation: true,
                maintainInteractivity: true,
                maintainSize: true,
                maintainSemantics: true,
                visible:
                    (widget.keepChildVisible ?? false) || !_showOverScreenChild,
                child: _child),
            Visibility(
                maintainState: true,
                maintainAnimation: true,
                // maintainInteractivity: true,
                maintainSemantics: true,
                maintainSize: true,
                visible: _showOverScreenChild,
                child: _overScreenChild),
          ],
        );
      },
    );
  }
}
