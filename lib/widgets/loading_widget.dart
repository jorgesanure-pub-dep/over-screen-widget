import 'package:flutter/material.dart';
import 'package:over_screen_widget/controllers/i_over_screen_controller.dart';

import '../over_screen_widget.dart';

class LoadingWidget extends OverScreenWidget {
  LoadingWidget(
      {Key key,
      Widget child,
      Widget Function(IOverScreenController) builder,
      bool isShowing,
      IOverScreenController controller})
      : super(
          key: key,
          child: child,
          overScreenContent: Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Builder(
                  builder: (context) {
                    final _loadingContainerSize =
                        MediaQuery.of(context).size.width * 0.4;
                    return Container(
                      width: _loadingContainerSize,
                      height: _loadingContainerSize,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.black12),
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
          builder: builder,
          isShowing: isShowing,
          controller: controller,
        );
}
