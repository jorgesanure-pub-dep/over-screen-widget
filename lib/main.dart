import 'package:flutter/material.dart';
import 'package:over_screen_widget/controllers/i_over_screen_controller.dart';
import 'package:over_screen_widget/over_screen_widget.dart';
import 'package:over_screen_widget/widgets/loading_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  IOverScreenController _loadingWidgetController;
  IOverScreenController _overViewWidgetcontroller;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Over Screen Widget Sample',
      home: Scaffold(
        backgroundColor: Colors.white,
        body: PageView(
          children: [
            _createLoadingWidgetDemo(),
            _createOverScreenWidgetDemo(),
          ],
        ),
      ),
    );
  }

  _createLoadingWidgetDemo() => LoadingWidget(
        builder: (controller) {
          _loadingWidgetController = controller;

          return FutureBuilder(
            future: _loadContent(),
            builder: (context, snapshot) => _createContent(),
          );
        },
      );

  _createOverScreenWidgetDemo() => OverScreenWidget(
        keepChildVisible: true,
        builder: (controller) {
          _overViewWidgetcontroller = controller;

          return FutureBuilder(
            future: _loadContent(),
            builder: (context, snapshot) => _createContent(),
          );
        },
        overScreenContent: Container(
          color: Colors.grey.withOpacity(0.5),
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      );

  _createContent() => Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Loaded screen. Ready to show content.",
              style: TextStyle(fontSize: 28),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              elevation: 5,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              color: Colors.amber,
              onPressed: _loadContent,
              child: Text(
                'Reload',
                style: TextStyle(fontSize: 28),
              ),
            )
          ],
        ),
      );

  Future _loadContent() {
    try {
      _loadingWidgetController?.show();
    } catch (e) {}
    try {
      _overViewWidgetcontroller?.show();
    } catch (e) {}

    return Future.delayed(Duration(milliseconds: 1300)).then((_) {
      try {
        _loadingWidgetController?.hide();
      } catch (e) {}
      try {
        _overViewWidgetcontroller?.hide();
      } catch (e) {}
    });
  }
}
